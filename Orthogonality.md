# Orthogonality
20200604194659

See also:
* [[20200604185357]] Dry - Don't repeat yourself

#programming-principle

Orthogonality is the decoupling and independence of a module. If changing code in one place causes an error somewhere else chances are the code is not orthogonal.

Orthogonality produces simpler code, easier to test, less bug prone and longer lasting code.
Simple test to see if system is orthogonal is to see how many other modules are affected by a change in the current module. best case scenario only one more module should be affected.

Careful relying on properties outside your control, setting phone number, postal codes or email addresses as the identifier makes you dependent on their formats

* *Keep your code decoupled* - Hide as much as possible, tell objects themselves to change their states
* *Avoid global data* - Even read-only is sensitive to multithreading, singleton is popular as global variables but can link to unnecessary linkage.
* *Avoid similar functions* - Similar functions is a symptom, maybe strategy pattern can help?

##### Testing

Testing can be a good test if system is orthogonal since it forces developer to break down the system in testable parts, if too many components needs to be used for a test the system might be coupled

##### Bugs

How many modules does a bugfix touch?
Does other bugs arrise from fixing a bug?
Report the number of files manipulated for each bugfix to count impact and health of system

##### Dry

Orthogonality and Dry is closely related

## Literature
* Thomas D. Hunt A. 20th Anniversary Edition - The Pragmatic Programmer 



