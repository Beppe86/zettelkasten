# LEGO Self-reflection
20240108125510

#Career #life

### **What were your key achievements, highlights and lowlights with respect to your performance.

Upon joining The LEGO group in August the PlayComm team had been without a tech lead and PO for a substantial period of the duration of the project.  

I was able to very quickly onboard and take over Scrum master, tech lead, and many PO responsibilities already from week one.

  

With the lack of a PO, I communicated with ConnectKits stakeholders to help refine and alter our priorities:  

* I started up a discussion with BB team, Roadrunner stakeholders, Jeremie and Mads Paulin in order to present a sober view on LCC integration and prevent resource waste.
* I opened up the communication channels with the firmware and app teams for Journey which really streamlines the releases and testing of the new features, there were particular challenges regarding BLE encryption, asset pack transfers, and Journey services.  
    
* Acquired and managed Joao Amaral as a QA developer to ensure long-term QA test coverage of ConnectKit and improvement of the test feedback loop.
* Facilitated a ConnectKit integration workshop for the team to find ways of improving the ConnectKit integration and how we can make it simpler.
* Frequent talks with Martin Bo and Cambridge Consultancy to make sure the people at Trifork were not blocked and had information and hardware readily available to quickly implement the WDX protocol.  
    
* Seeking clarity in P11 world where clear directives can be very hard to attain, I have spent much energy trying to connect the dots and trying to figure out how ConnectKit can contribute to the technical landscape without blindly trying to impose ConnectKit or acting gatekeeper to the Connectivity domain.  
    

  

I see my initial responsibility in PlayComm to act as tech lead for ConnectKit but the majority of my time I instead had to fulfill many PO responsibilities as you can see above, so many that I had to enable Mads Moller in an architectural role over ConnectKit. I think this solution has worked out fine and I am still very close and deeply knowledgeable of the tech stack but it highlights the wide spectrum of responsibility I have shoulder already during my first semester.  

  

As the tech lead of ConnectKit, I consider much of my success to be good communication and presence to my team:

* With a mix of prioritizing, cleaning, and task creation I was able to appoint feature leads that could stay focused in smaller domains.
* With Joao included I have run a team of up to 9 people, I have dealt with conflicts within the team and also given much support and confidence to struggling members. I see the culture of the team which is very accepting of mistakes as proof of this work.  
    
* Right from the start I have prioritized going to the Trifork office once a week instead of working more from home. It would have been calmer and less taxing to work more from home instead but I believe a physical presence as a leader is important, Especially since the majority of the team is consultants and it very easily fosters a 'we and them' culture otherwise.  
    
* Although Cambridge Consultant was delayed and much of their work came in tidbits PlayComm was able to integrate a completely new protocol in less than 3 months, this was also being done while still supporting Journey and building up ConnectKit as a platform!
* We have fully integrated Journey and in general received good feedback on our platform, it seems stable and flexible enough that Journey has been able to alter much behavior after a pivot from the firmware team to keep all services behind the game engine. This is without any support from QA!  
    
* I have also facilitated most retrospectives and other scrum ceremonies.

  

Things that could have been done better:

* It was only in November that a clear roadmap was established, this was partially because of unclear requirements from our stakeholders but that is really no excuse to not set our own timeline.
* The documentation was left behind and could still be improved upon.
* The work that was required for the LCC integration took much time to gather and I wonder if we took a too academic approach to it and should have started with more spikes to experiment.  
    
* After I gave Jeremie my perspective for Q1's SoW I am still critical to what work we need to do in Q1. This could have been mitigated by even more proactive communication with key stakeholders in 'P11 land'.

### What behaviors helped you succeed or progress towards your deliverables? What behaviors could you have displayed more to achieve even better outcomes?

I am not the most technically knowledgeable person on the team, I understand and plan code architecture very well but PlayComm has to deal with a multi-layered stack where on each layer there is probably someone who knows details better than I do.  

Behaviors that have helped me:

* Delegation of responsibility. -All initiatives have had a designated lead who has been able to help me work out tickets, and join me in meetings and in sparring for solutions.
* Supportive and non-judgemental. -For team members to take on responsibility I wanted to make sure they were not afraid of making mistakes. I focus on encouragement for their work and constructive feedback. I also have made sure I have had a physical presence both at the Trifork office and Billund.  
    
* Interdepartmental networking. -I have tried to create bonds and connect with key people at different teams and key positions to ensure ConnectKit stays relevant. ConnectKit is a mid-layered component with stakeholders both above and below in the architectural hierarchy. This requires much communication and proactive behavior to not always end up being a bottleneck.  
    

  
Behaviors I could display more of:  

* Sometimes a full schedule or in-depth technical discussions have made me slow to respond to inquiries and chat messages. I intend for 2024 to be more aware of my time and include more breaks so I can stay present in team chat messages and phone calls.  
    
* We developed a roadmap and I intend to keep it up to date to easier convey our progress.

### What are your key learnings from the past year  and what have you started doing differently as a result?
### Based on these and previous learnings, what do you consider to be important focus areas for your development in the coming year?

I need to accept that things within Lego take time as it is a slow organization. In the beginning, I scheduled meetings with short notice and was very adamant about getting a hold of people quickly so as to not be held back by important decisions. With a more clear direction for ConnectKit and a defined roadmap, it is easier to accept the pace that the Lego group is working with.  

I also realised that many times when I could not find an answer to a question it was because no one had an answer. I believe with this in mind, I can have a bigger impact on decisions made around my domain if I stay proactive and talk about our challenges with people in teams around us.

### To set you up to grow and succeed in the coming year what are your specific asks of your people leader or others (specify which stakeholders, where relevant)?

### Do you have any other related input you want to share with your People Leader?

The position I held at Trifork by the end was a team lead role and we were in talks of giving me many of the new direct reports to alleviate the leadership team. I also participated in some leadership courses to prepare me for this transition.  

I know Lego is a bigger organization and responsibilities take time to solidify but it is important for me to highlight my career aspirations as they have not changed since I left Trifork.