# Dry - Don't repeat yourself
20200604185357

See also:
* [[20200604185357]] Orthogonality

#programming-principle

Dry is more than just preventing copy and paste, it's about preventing copying of _knowledge_.

* *Duplication in code
* *Duplication in documentation* - not all code needs documentation, many times documentation compensates for bad naming. Ask yourself, what knowledge does the comment add?
* *Dry violations in data* - Storing calculated values adds more maintenance, is error prone and violated the dry principle. To cache values, calculated it once and hide with accessors. End user should not know if a property is calculated or stored
* *Representational duplication* - APIs often forces duplication of schemas, can be mitigated:
    * Internal APIs - Define schema in neutral format (JSON maybe?) and auto generate representation , tools might also help with tests, documentation, mock API and even complete API clients in chosen language.
    * External APIs - Often times OpenAPI versions exists. Otherwise, maybe implement and share to get help with maintaining?

## Literature
* Thomas D. Hunt A. 20th Anniversary Edition - The Pragmatic Programmer