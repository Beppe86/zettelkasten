# Personal Retrospective
20220629130212

See also:
 *  [[20201106153030]] ### Work ###
 *  [[20201006090639]] Routines
 *  
 
#stress #life #routines

# Work Performance
### Have I done something extra ordinary for Trifork that should be noted down this week?
* Raise flag for Flemming regarding milestones - BBC and Droid Commanders
* Lot's extra communication to get Unity 2021 upgrade with all components
* Discussed with Tue team needs
    * Team kept us at arms length
        * Beate did not involve in roadmap
        * Nikolaj did not invite in meetings
    * Prolong SOW to summer
    * Estimate and roadmap
* Discussed with Jakob strategies for COMs

# Stress

## Status
### Green
* I have projects I am working on during my free time
* I have ideas
* More interesting sex life
* More enthusiasm when I have Oliver
* Mood: 7/10
* I have exciting things to look forward to
* Delayed bedtime should be because of fun things like games or projects, not fatigue

### Orange
* Heart race
* Delayed bedtime because of fatigue or lack of discipline
* Not looking forward to following days
* Want to be alone for more than 2-3 days maybe?
* Crashing when I get home

### Red
* Procrastinate simple TODOs that should just be done 
* Heart race
* pressure on the chest
* Feel many expectations
* Don't make use of pauses throughout the day
* Bad conscious of things I have not done or don't have time to do
* Tough with myself and any lack of progress
* Mood: 4/10
* Dry skin
* Random pain in joints or jaw
* Headache
* Failing memory
* Anxiety attacks

## What to do

* Bekymringsrum
* Accept my needs and desires and prioritise them accordingly
    * They could be said out loud
    * Demand and request needs
* Remember positive activities like:
    * Lego
    * Program
    * Games
    * Friends
    * Exercise/Sports
    * SCA
* Talk about feelings and stress with friends
* Be honest at work towards myself and my colleagues
    * Mention lack of knowledge
    * Mention when things took longer
    * Be humble towards myself and don't treat colleagues too carefully
* Leave work on time
* Translate worries into TODOs