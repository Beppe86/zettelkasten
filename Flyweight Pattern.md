# Flyweight Pattern
20221229120435

See also:
 * [[20221209173109]] Composite Pattern
 * State
 * [[20221212102326]] Strategy Pattern
 
#design_patterns #structual_pattern
 
Use sharing to support large numbers of fine-grained objects efficiently.

## Applicability