# Decorator Pattern
20221210140326

See also:
 * [[20221209173109]] Composite Pattern
 * [[20221212102326]] Strategy Pattern
 * Adapter Pattern
 
#design_patterns #structual_pattern
 
 Attach additional responsibility to an object dynamically. Decorators provide a flexible alternative to subclassing for extending functionality

## Applicability
* Add responsibilities to objects dynamically and transparent
* responsibilities that can be withdrawn
* Where Extension by subclassing is impractical

## Participants
* **Component**
    * Defines shared interface for objects that can have added responsibility
* **ConcreteComponent**
    * Object to which added responsibility can be added
* **Decorator**
    * Keeps reference to component defines interface to conform components interface
    * Decorators forwards requests to component and can add functionality before or after such call
* **ConcreteDecorator**
    * Adds responsibilities to component

![d0d17d95528dc0cea85f6894176ac293.png](d0d17d95528dc0cea85f6894176ac293.png)



## Consequences
* More flexibility than static inheritance
* Avoids feature heavy structure high in the hierarchy
* Remember that a decorated component is not the same as it's component, type matching returns false
* Might create the feeling of a bloated system, especially for new eyes

## Implementation
* The abstract decorator can be omitted for a more lightweight solution
* Keep the component (abstract) class lightweight, all class will inherit from it. Storage should be closer to it uses
* Consider using [[20221212102326]] Strategy Pattern if the Decorator class because to complex

![0d67f0e0fa3ee9aac7f6b8b1d796af01.png](0d67f0e0fa3ee9aac7f6b8b1d796af01.png)

## Related Patterns
* Adapter gives an object a new interface
* [[20221209173109]] Composite Pattern, decorator adds additional responsibility outside a composites aggregation
* [[20221212102326]] Strategy Pattern changes the internal workings as opposed to decorator which applies changes before or after

## Literature
* Design Patterns - Page 175