# Tracer bullets
20200605172746

See also:
* [[20200611174359]] Prototype

#programming-principle
 
 A sort of MVP where early development focuses on pushing a feature through the full stack. Once done and approved by stakeholders the trace can be fleshed out more and additional feature can be added
 
 The way a tracer bullet differs from a prototype is that a tracer bullet is expected to grow into the final product were a prototype is expected to be discarded once the purpose of the prototype have been fullfilled, this of course changes the quality of code and documentation written for a tracer bullet compared to a prototype
 
 
## Literature
* Thomas D. Hunt A. 20th Anniversary Edition - The Pragmatic Programmern