# Composite Pattern
20221209173109

See also:
 *  Chain of Responsibility
 *  [[20221210140326]] Decorator Pattern
 *  Flyweight
 *  Iterator
 *  Visitor
 
#design_patterns #structual_pattern
 
 Compose objects into tree structures to represent part-whole hierarchies. Composite lets clients treat individual objects and compositions of objects uniformly
 ![1ac75eea599b32fceddce07404d9c28f.png](1ac75eea599b32fceddce07404d9c28f.png)
 
 
## Participants
 * **Component**
     * declares the interface
     * implements default behaviour
     * (optional) defines access to parent
 * **Leaf**
     * Has no children
 * **Composite**
     * stores child components
     * implements child related operations
 * **Client**
     * access the composition through component interface

![774e20114a1d43a7b9885cabdc346431.png](774e20114a1d43a7b9885cabdc346431.png)



##  Consequences
* Clients should not care if they deal with composite or leaf
* Can make design overly general, harder to restrict

## Implementation
* Maintaining reference from child to it's parent can simplify traversal
* Sharing components can be difficult if parent is referenced
* Child management could be place on component or composite (transparency vs safety) 
* Caching could be implemented to improve performance

## Related Patterns
* Component-parent link is used for Chain of Responsibility
* [[20221210140326]] Decorator Pattern is often used with Composite
* Flyweight lets you share components
* Iterator can be used to traverse composites
* Visitor localises operations that would otherwise be distributed across Composite and Leaf classes

## Literature
* Design Patterns - Page 163