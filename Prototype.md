# Prototypes
20200611174359

See also:
* [[20200605172746]] Tracer bullets

#programming-principle
Prototypes can be used to test out new features. Commonly the product of a prototype is not the code but the experience from making it.
Careful if the details of the prototype takes up too much time, in this case a Tracer bullet might be a better design principle.

It is not uncommon to write a prototype in a different language to help management easier resist the temptation of making the prototype the actual product. A high level language could increase efficency  'The Pragmatic Programmer' recommends Python or Ruby

This suitable for prototype:
* Architecture
* New functionality in an existing system
* Structure or contents of external data
* Third-party tools or components
* Performance issues
* User interface design

Don't spend too much time on details when developing the prototype, dummy data and incomplete features might very well be enough. Error handling and correct code patterns and or UI might could also be disregarded. What is relevant though is the documentation. Without documentation the very problem the prototype is supposed to solve might reappear when actual production begins.

### Architecture
Architectural prototypes might only contain interfaces and modules without actual implementations or maybe just on a whiteboard with post it notes.

Things to look for:
 * Responsibilities defined?
 * Collaborations between components defined?
 * Prevent coupling
 * Prevent duplication
 * Interface definitions and constraints
 * **Does every module have access to data it needs WHEN it needs it?**

## Literature
* Thomas D. Hunt A. 20th Anniversary Edition - The Pragmatic Programmer