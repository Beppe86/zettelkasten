# TODO - Work

See also:
[[20201104225102]] TODO - Arpgmy
[[20201116113350]] TODO - Private
[[20220629130212]] Personal Retrospective

[[20201006090639]] Routines

# Notes
### Leadership course 27 March TODO:
* Book transport
* Interview before start date:
    * Soren
    * Jakob
    * Mads
* Verify everyone answered questioneer before 

# Retrospective
* We need to prepare for sprint reviews
* Struggle with BBC, everything is just copied over
* Nothing works on BBC branch
    * asmdefs
    * widgetrefs
    * Tests
        * In the wrong place (Playmode/EditMode)
        * Fails
        * Missing assemblies
        * Entered in coding mode instead of activity
    * Gitflow!
    * Run the tests!

    
# Level up
* LevelUp
    * Leadership
        * Management
            * Study group
                * Best practices
                * Zenject
                * Open Mindset
                * Who moved my cheese
                
        * Writing skills
        * Personalities (Myers & Briggs)
            * Trifork Trine holds course
        * Scrum master
            * Course
    * Developer
        * Unity 
            * Certificates
                * Lightning
                * Animations
            * Samuels project
        * Architecture & Patterns
        * CI/CD - Yammel - Gitlab
    * Academia
        * Math
        * Computer science course
        * UML/Codeflow diagrams

# Reading
* 7 Habits

# Information gathering
* Title: Unity ML-Agents 2020 Wrap Up + 2021 Look Ahead Link: [https://blogs.unity3d.com/2020/12/28/happy-holidays-from-the-unity-ml-agents-team/](https://blogs.unity3d.com/2020/12/28/happy-holidays-from-the-unity-ml-agents-team/) Categories: Machine Learning, 2020, 2021
* Title: Feature Preview Webinar: Localization Package Link: [https://youtu.be/qmpswjJuhoc](https://youtu.be/qmpswjJuhoc) Categories: 2020.2, Beta, Editor 
* Title: A Flexible Game Architecture with Scriptable Objects Link: [https://www.youtube.com/watch?v=WLDgtRNK2VE](https://www.youtube.com/watch?v=WLDgtRNK2VE) Categories: Unite Now, Community, Scripting/Programmers 
* Title: Display Profiler Stats at Runtime Link: [https://youtu.be/i2IpJHUyZLM](https://youtu.be/i2IpJHUyZLM) Categories: Scripting/Programmers, Profiling, 2020.2 
* Title: New HDRP Template Link: [https://blogs.unity3d.com/2021/01/07/explore-learn-and-create-with-the-new-hdrp-scene-template/](https://blogs.unity3d.com/2021/01/07/explore-learn-and-create-with-the-new-hdrp-scene-template/) Categories: Graphics, Editor, 2020.2, Getting Started with HDRP

# Team
* Analysists
    * Architect - https://www.16personalities.com/intj-personality
        * Samuel 
    * Debater - https://www.16personalities.com/entp-personality
        * Niels
        * Beppe
    
* Diplomats
    * Mediator - https://www.16personalities.com/infp-personality
        * Niclas 
        * Andreas
    * Campainer - https://www.16personalities.com/enfp-personality
        * Astrid

* Sentinels
    * Logistician - https://www.16personalities.com/istj-personality
        * Alexander
    * Consul - https://www.16personalities.com/esfj-personality
        * Jacob
        * Mads
