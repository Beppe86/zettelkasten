# Teambuilding
20210406181741

See also:
[[20201106140155]] If I were team lead
[[20201106153030]] ### Work ###

### Indoor activites
* Assemble Lego in pairs with one looking at the instructions and one assemble the pieces
* Unbeliveable fact abut themselves - group should reach a consensus
* Three facts and one lie
* Roborally

### Topics
* Personality types
    * Myers Briggs
    * Personal Strength/Weakness
    
   1½ planning 
    
## Schedule v1
 * 0900 - Breakfast
 * 0945 - Unbelivable fact
 * 1030 - Personal sharing (cont.)
     * Explain your personalities strength and weaknesses (Real world Examples maybe?)
     * Few minutes to tell how each person was as a child. (Special interests and bad behaviours?)
* 1130 - Lunch
* 1230 - Personal sharing (cont.)
* 1300 - Assemble Lego
* 1330 - Roborally team game
* 1500 - End?

## Schedule v1
* Breakfast
* Unbelievable facts
* Discuss Aspects
* Lunch
* Personal sharing
* Competition

# Personal Talking points
* How were you as a child?
    * What have changed and what remains the same?

* Weakness
    * How have you improved?

* Strengths
    * What gives you motivation?

#### Personal Talking points
* Childhood - I was a rebel and obnoxious
    * Pushed peoples buttons
    * Challenging everyone
    * Loyal
    * Became much more humble

* Weakness
    * I am hard on my self
    * Insensitive
    * Argumentative

* Strength - Enthusiastic and supportive
    * I want to bring positivism
    * I love criticism in almost all shapes - No one is harder than my anyway 
    * I have work hard to negate my weaknesses


## Myers Brigg Questions

###  Aspects
#### Energy - How we see the world and process information
* Intuitive
    * Dreamy
    * What if?
    * Leap too quickly into matters
    * Innovative
* Observant
    * Do what works
    * Present - not future or past
    * Practical
    * Focused
    * Doers
    * (Less open minded)

Scope creep
Workshops
Pair programming

#### Nature - How we make decisions and cope with emotions
Left handed vs Right handed - same with Thinking vs feeling. We do both
* Thinking
    * Objective
    * Insensitive
    * discomfort with emotions
* Feeling
    * Compassionate
    * Protective
    * burn out from too involved

Office only with one type
Friday hygge meeting
Code reviews
Personal inconvinience - how do and should other react?

####  Tactics - Our Approach to work, planning and decision-making
Spontaneity or certainty
* Judging
    * Mr backup plan
    * Checklists
    * Once commited to a task it's not open to reconsideration
    * Rigid
    * Does not like surprises
    * Rules and laws
* Prospecting
    * Ready for change
    * Seize unexpected opportunities
    * Reactive instead of controlling
    * Impulsive
    * Unfocused

Changed priorities
Move to a new project

#### Mind - How we interact with our surroundings
* Introvert
    * Listeners
    * Reading situations
* Extrovert
    * Initiative
    * External Validation

Office environment
Lunch breaks
Meetings


