# Routines
20201006090639

See also:
* [[20200818081715]] Wanted efficiency tools-hotkeys
* [[20200824152331]] Zettelkasten tips
* [[20220629130212]] Personal Retrospective


### I should do this every time I start a day
* Note down arrival time
* Look at calendar
* Start Zettlr
* Look at TODO

### Things I should keep in mind during the day
* Remember to document!

### I should do this every time I leave for the day
 * Note down leave time
 * Look at the calendar for tomorrow