# MacOS QoL
20220909084255

See also:
 * 
 
#macos #qol
 
# Amethyst hotkeys
* Focus screen 1: ⌥⇧ + W
* Focus screen 1: ⌥⇧ + E
* Focus screen 1: ⌥⇧ + R

* Move window to screen 1: ⌃⌥⇧ + W
* Move window to screen 2: ⌃⌥⇧ + E
* Move window to screen 3: ⌃⌥⇧ + R

* Select tall layout: ⌥⇧ + A
* Select wide layout: ⌥⇧ + S
* Select fullscreen layout: ⌥⇧ + D
* Select column layout: ⌥⇧ + F

* Refresh: ⌥⇧ + Z
* Toggle Float: ⌥⇧ + T
* Move focus clockwise: ⌥⇧ + K
* Move focus counter clockwise: ⌥⇧ + J

* Swap window clockwise: ⌃⌥⇧ + K
* Swap window counter clockwise: ⌃⌥⇧ + J