# Leadership thoughts
20201106140155

See also:
[[20230525084912]] Coaching from the back of the room

### Sprint start
* Have team estimate tasks right from the start
    * Later on add kickoffs based on a certain point cutoff (For example 3 points)

# Where am I heading
* Working towards people management more than tech lead
* 

# Courses and needs
* Great information at: https://rework.withgoogle.com/

## Informal Leader
* Tools to communicate constructively
* Establish credibility
* Inspire motivation
* (Do I need to consider more certain boundaries?)
* Personal feedback and techniques for good follow up questions
* Constructive criticism
* Faster and more correct grammar
* Naturally transition from my old role to a leader
* Team dynamics
* Remove friction
## Team composition
* Balanced team of Rock Stars and Superstars

##  Relationship
### Culture of guidance
* Praise
* Criticism

### Motivation
* Avoid burnout
* Avoid boredom

### Results
* Ownership

# Leadership course Mannaz - Teamledelse 27. March

## Body Language
* Bend knees and lean forward - Child like, not dangerous
* Open palms - Invitation
* Arms behind the back - Power
* Move stuff around on 'stage' - Power
* Hands above shoulder - Energy
* While pointing on a whiteboard it can either be hard pointing or soft rotating pointing

#### What body language to use
* Feet apart but many guys keep them too far away from each other
* Move to the other side of the 'stage'
* All hand movements should happen in front of the hips
* Hand movements should be symmetrical

## Feedback on my presentation
* Icebreaker like Beppe was good
* I speak loud which signals confidence which many times are good but not always
* Maybe I should not be talking myself down in self irony. I said 'Don't hold it against me' and 'Crappy Danish'
* I was very laid back, I should be aware of this and decide when it's appropriate and not

## When we are giving feedback
* We should describe what we saw objectively and then inform how we interpret it. : When I say your hands behind your head I felt like it had no meaning or purpose
* Always start by giving them something positive

# Tuckmans Team phases
## Forming
* Do I fit in?
* May I take part?
* Who are the others?
* What do we have in common?
* What are we going to do?
* How should we do it?
* Who do we answer to?
* Identified as
    * Hygge
    * Polite questions

### How should the leader act Forming
Everyone figures out if they are 'in' or 'out' and the leader should try to create as many openings as possible for members to find similarities in between each other

* Make clear goals
* Remove uncertainty
* Help them get to know each other (fast)
* Create rules and norms
* Provide easy tasks
* Don't be democratic
* Stricter body language

One exercise ones explained to me named Cultural History which helps people find commonalities. I hope I will find the paper from the Mannaz course to explain more in detail here 

## Storming
* Nervous
* Scary
* Plays for power
* Who is in charge?
* What role can 'I' get
* Establishing of common understandings, roles and routines

### How should the leader act in Storming
Team members find their place in the hierarchy.

* Dare to join conflicts without taking part
* Give space for differences
* lead or facilitate conflicts
* Leave space for diversity and different ways of working
* Encouragement and positive feedback is paramount here

## Norming
* Set rules and norms, examples could be what type of language would be used, do people show up on time and is it OK to interrupt when others are talking.
* What can we agree upon?
* Procedures and ways of working are set
* More open to differences in the team
* Conflicts are less scary
* More open for feedback
* Make use of more 'team language'

### How should the leader act in Norming
In this and Performing phase does it get decided how tight a group will become, maybe they will be reserved colleagues or a friend group for life?

* Facilitate the decision process
    * Rules
    * Norms
    * Ways of working
    * Standards
    * Language
    * Procedures
* Enforce the decisions for example through contracts
* Support the group identity
* Refine, communicate and enforce common decisions

## Performing
* Social calm
* Insight in tasks
* Self organising
* What can be achieved together?
* How tight will we be?
* Team produces
* High performance is created through being dynamic and the teams capabilities for self reflection and establishing of trust and reciprocity
* Work effectively alone and in groups
* Show interest and engagement in each others work
* Inspire and challenge each other
* Reflects work done in group

### How should the leader act in Performing
In this and Performing phase does it get decided how tight a group will become, maybe they will be reserved colleagues or a friend group for life?

* Delegate leadership
* Maintain a frame or benchmark for productivity that can be improved
* Challenge the team academically
* Create space for reflection and feedback

## Ajourning
* Nostalgia
* Proud
* Chock
* Pessimism
* Anger
* Resignation
* Have we created anything meaningful together?
* Close down parts of or the whole team
* Say farewells and handovers

### How should a leader act in Ajourning
 * Leave room for reactions and feelings
 * Facilitate closure
 * Delivery and handover
 * Point out what the team have achieved and created
 * Leave space and maybe contribute with closure routines to create meaning

# Leadership role
Management as a difference to leadership is about manage complexity like:
* Planning, scheduling and budget
* Organising and manning
* Control
* Problem solving
It explains the 'How'

Leadership is about creating change like:
* Set direction
* Motivate and inspire
* Alignment of people
* Educating and developing people
It explains the 'Why'

A lower level leader spends much of their time solving technical problems and make use of their branch knowledge but the higher the leader is much of that time is instead spent on strategy and overview

Statistically a new leader starts by trying to pleasure their team members and in the 6 first months a new leader focuses mostly on the companies demands and expectations, they forget to connect with peers and put their own needs on the side.  
Many leaders stop after 1 year, the ones who stays creates a platform to stand on where they can filter needs from below and the top. Try to find leaders you look up to and learn what they do.

# Hiring
* Emotional intelligence can be identified with personality tests where the resulting profile is irrelevant but how they talk about themselves and if they can explain and identify their own faults and behaviours indicate an emotional intelligence
* How capable are they to join a team? - This measures the social competence of the person
    * Ask how they act in a new team
    * What would they focus on?
    * What problems could arise when you or any  new member joins a team?

The core competences of a person would be their academical expertise followed by in order of importance: Emotional intelligence, Social intelligence, team knowledge and finally business knowledge.

# High Performance Team
A hpt team can manage 7 team members plus minus 2
