# Lightsail
20220612134751

See also:
 * [[20220608103347]] VPC
 
 #aws #aws_tutorial #container
 Guide: https://aws.amazon.com/getting-started/guides/deploy-webapp-lightsail/
Lightsail is an virtual private server (VPS) provider for an application or website.
