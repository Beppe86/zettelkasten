# Jammer Drills
20200625144912

#Jammer #DerbyDrills

### Apex jumps
- Blocker facing jammer behind opposing blocker, pull in the jammer when he lands
- Blocker claims the line preventing opposing blocker from holding the line, might help jammer with landing
- Limiting the width of the track so only line 1, 2 and 2 ½ are passable and the other part is covered with cones. Jammer can jump, hit or go around on the outside, the idea is to practice feints.

### 