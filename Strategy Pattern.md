# Strategy Pattern
20221212102326

See also:
 * [[20221210140326]] Decorator Pattern
 * Flyweight Pattern
 
#design_patterns #object_behavioral_pattern

Encapsulate each member in family of algorithms and make them interchangeable. Strategy lets the algorithm vary independently from clients that use them.

## Applicability
* Provides a way to configure a class with one of many behaviours
* When you need different variants of an algorithm
* When an algorithm uses data that clients shouldn't know about. Strategy pattern avoids exposing complex, algorithm-specific data structures
* If a class defines many behaviours and these appear as multiple conditional statements. Instead of many conditionals move related conditional branches into their own Strategy class.

## Participant
* **Strategy**
    * Declares an interface common to all supported algorithms. Context uses this interface to call algorithm defined by a ConcreteStrategy.
* **ConcreteStrategy**
    * Implements algorithm using the Strategy interface
* **Context**
    * Configured to use a ConcreteStrategy
    * Maintains a reference to Strategy object
    * May define an interface that lets Strategy access its data

![dffa55483e85af510bc28530c70b6c84.png](dffa55483e85af510bc28530c70b6c84.png)


## Collaborations
* Context may pass all data required by the algorithm to the strategy when called alternatively the Context passes itself as an argument to the Strategy
* Clients pass ConcreteStrategy to the Context and then exclusively interacts with the Context.

## Consequences
* Inheritance can be useful on family of strategies, this have the added benefit of removing subclassing from Contex
* Strategies eliminates conditional statements
* Choice of implementations. Client can choose between time and space trade-offs.
* Forces client to know there are multiple options of strategies available
* There will be more communication overhead between Strategies and Context, with multiple strategies there will be information sent into the strategies that not all strategies make use of.
* Increased number of objects in project.

## Implementation
* Consider what data is needed across all strategies.
    *  Have Context pass data in as parameter on Strategy calls, this keeps Strategy and Context decoupled, this might pass data the strategy wont need.
    * Alternatively pass Context itself as a parameter to the Strategy, Strategy can then request data on demand explicitly. Or store the context for later needs. This couples Context and Strategy closer together.
    * An option can be to use a generic on Context to indicate what Strategy will be used

![98c07b90abff79a07d2b487737bd07b1.png](98c07b90abff79a07d2b487737bd07b1.png)

## Related Patterns
* Flyweight Pattern: Strategy objects often make good flyweights.

## Litterature
* Design Patterns - Page 315